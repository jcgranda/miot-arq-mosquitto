# Imagen Docker de Mosquitto para el Máster en Internet de las Cosas #

Imagen Docker de Mosquitto que se utiliza en la asignatura Arquitecturas de Protocolos del Máster en Internet de las Cosas de la Universidad de Oviedo.

### Arranque del contenedor ###

Para iniciar el contenedor:

```console
sudo docker-compose up
```
